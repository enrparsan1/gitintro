# Buenas Prácticas en el Desarrollo de Proyectos

Introducción al control de versiones con ``git``

# Contenidos

- [Contenidos](#contenidos)
- [¿Qué es el control de versiones?](#qué-es-el-control-de-versiones)
- [Áreas de trabajo en ``git``](#áreas-de-trabajo-en-git)
- [Configurando ``git``](#configurando-git)
- [Git help](#git-help)
- [Git básico](#git-básico)
- [Mensajes de commit](#mensajes-de-commit)
- [Diff y log](#diff-y-log)
- [Recuperando versiones anteriores](#recuperando-versiones-anteriores)
- [Trabajando con el repositorio remoto](#trabajando-con-el-repositorio-remoto)
- [Etiquetando](#etiquetando)
- [Resolución de conflictos](#resolución-de-conflictos)
- [Branching y merging](#branching-y-merging)
- [Otros comandos útiles](#otros-comandos-útiles)
- [Agradecimientos](#agradecimientos)
- [Referencias](#referencias)


# ¿Qué es el control de versiones?
El control de versiones, también llamado control de revisiones o control de código, es la gestión de los cambios que se van realizando sobre ficheros de código, texto, u otro tipo de información. 

Un sistema de control de versiones es un software que permite gestionar los cambios que se van realizando sobre un conjunto de ficheros, de forma que se puedan recuperar en cualquier momento versiones válidas del código, etiquetar versiones específicas, y permitir el trabajo simultáneo de diferentes desarrolladores. Los sistemas de control de versiones pueden y deben usarse en proyectos que trabajen con código software (C, C++, Java, etc...), lenguajes de descripción de hardware (VHDL, Verilog, …), documentos (texto plano, Latex, etc...), … Se puede poner cualquier tipo de fichero bajo control de versiones, aunque los ficheros de texto son más sencillos de manejar. Normalmente no se incluyen binarios que pueden generarse a partir de ficheros fuente que estén en el repositorio.

Existen multitud de herramientas diferentes de control de versiones. En esta práctica vamos a usar ``git``, pero también existen otras herramientas como ``svn`` o ``mercurial``.

# Áreas de trabajo en ``git``

![Áreas de trabajo en ``git``](areasdegit.png "Áreas de trabajo en ``git``")


La arquitectura de ``git`` define varias áreas de trabajo. Es importante conocerlas para entender bien los mensajes que nos pueda dar la herramienta:

- **Repositorio remoto**: es una copia completa del historial de versiones del proyecto. Contiene información suficiente para reconstruir cualquier versión del mismo.

- **Repositorio local**: es una copia completa del repositorio remoto. Al igual que el repositorio remoto, contiene toda la información del proyecto.

- **Workspace** (espacio de trabajo). Es el directorio en el que trabajaremos. Se ve como un directorio más de nuestra máquina.

- **Stage** (o index): es una caché de los ficheros que se van a añadir o cambiar en el repositorio local. En el repositorio local no se copian los cambios que haya en el workspace directamente, sino que primero se añaden a stage y luego de stage se copian en el repositorio local. Esto será importante para entender el funcionamiento del comando ``git add``.

- **Stash**: es un área temporal donde se pueden esconder temporalmente cambios mientras se trabaja en otra cosa.

A continuación, veremos unos comandos básicos para manejarnos con ``git``.


# Configurando ``git``

La configuración de ``git`` puede editarse en tres ficheros diferentes:

  - A nivel de sistema:          ``/etc/gitconfig (en Linux)``
  - A nivel de usuario:          ``~/.gitconfig``
  - A nivel de repositorio:      ``<path_al_repo>/.git/config``


La configuración específica de repositorio tiene prioridad sobre la de usuario, que a su vez tiene prioridad sobre la de sistema.

Editamos ``~/.gitconfig``, que es la configuración a nivel de usuario, con el editor de texto que prefiramos (por ejemplo ``nano`` o ``gedit``).


Un fichero de configuración de ejemplo sería:

    [user]
        name = Nombre Apellido
        email = usuario@correo.dominio
    [core]
        editor = nano
    [color]
        ui = auto
    [push]
        default = matching

Para comprobar la configuración que está viendo ``git``, puedes hacer:

    git config --list

# Git help

En esta sesión no va a dar tiempo a que veamos todo ``git`` en profundidad. Algunos comandos pueden recibir más argumentos de los que veremos aquí. En caso de duda, puedes utilizar ``git help`` para obtener más información sobre ``git`` o sobre algún comando concreto:

    git help [comando]

# Git básico

Lo primero que haremos será clonar un repositorio remoto. Al clonarlo, crearemos el repositorio local, sobre el que trabajaremos, y se creará el workspace (directorio de trabajo).

Para clonar el repositorio de nuestro grupo:

    git clone https://usuario@gitlab.com/ruta/al/repo.git

Una vez clonado el repositorio, los comandos básicos para trabajar con el repositorio local son:

Añadir el fichero al control de versiones, realizando una copia del mismo en el stage:

    git add <fichero>

Eliminar un fichero del control de versiones:

    git rm <fichero>

Mover un fichero:

    git mv <fichero> <destino>

Mostrar el estado actual del repositorio, indicando los ficheros modificados y si existen diferencias entre el repositorio local y el remoto:

    git status

Añadir los cambios que estén en el stage al repositorio local:

    git commit

# Mensajes de commit

Es muy recomendable que el mensaje de commit siga el siguiente formato, para que el log resultante sea entendible:

- La primera línea debe tener, como máximo, 50 caracteres, y debe ser un resumen conciso de los cambios.
- Si vamos a añadir una descripción más extensa, la segunda línea debe dejarse en blanco.
- A partir de la tercera línea, se puede opcionalmente realizar una descripción más concreta de los cambios realizados. Estas líneas deben tener 72 caracteres como máximo.

# Diff y log

Mostrar los cambios no añadidos al stage, es decir, las diferencias entre working directory y stage. Si no se especifica ningún fichero, muestra todas las diferencias:

    git diff [fichero]

Mostar el historial de commits:

    git log

Mostrar el historial de commits para un fichero específico:

    git log <fichero>

Mostrar el historial de commits, para todas las ramas, con un commit por línea y en modo grafo:

    git log --all --oneline --graph

# Recuperando versiones anteriores

Antes de hacer un commit, si hemos añadido un cambio que no queremos que se copie al repositorio local, podemos hacer lo que se conoce como un “unstage”, es decir, quitar del stage los cambios añadidos:

    git restore --staged <file>

También podemos recuperar la versión del repositorio local de un fichero, si hacemos:

    git restore <file>

Si no queremos la última versión, sino una anterior, podemos hacer:

    git checkout <checksum> <file>

Lo que recuperará la versión del commit indicado por el checksum.

Además, podemos pasar los checksum de los commits a ``git diff`` si queremos comparar entre commits específicos. Se puede ver la sintaxis concreta haciendo ``git help diff``.

# Trabajando con el repositorio remoto

Para comunicar el repositorio local con el remoto utilizaremos los comandos pull y push:

Descargar cambios (commits) del repositorio remoto:

    git pull

Empujar tus commits al repositorio remoto:

    git push

Un esquema típico de una sesión de trabajo con un repositorio remoto sería el siguiente:

![Esquema de una sesión de trabajo con ``git``](sesiontrabajo.png "Esquema de una sesión de trabajo con ``git``")


De esta forma, en primer lugar se descargan los cambios más recientes del repositorio remoto (pull). Sobre estos cambios se trabaja, y localmente se van realizando commits (y restore/checkout cuando sea necesario). Finalmente se hace push y se suben los cambios al repositorio remoto. Por supuesto, se puede hacer push y pull más veces si es necesario, por ejemplo, si necesitamos descargar cambios que acaba de hacer otro miembro del equipo.

# Etiquetando

Una etiqueta refiere a una versión específica del código. Es interesante utilizarlas para no tener que referirnos a una versión concreta con un hash como “a719fdaed776006a2fefe80267c445514d92a6bd”.

Existen etiquetas ligeras (lightweight, por defecto) y anotadas (annotated). Estas últimas pueden llevar un mensaje.

Para mostrar las etiquetas:

    git tag

Para crear una etiqueta anotada:

    git tag -a <nombretag> [checksum]

Las etiquetas no se envían por defecto al repositorio remoto. Si queremos enviar la etiqueta al repositorio remoto tenemos que ‘pushearla’ explícitamente:

'Pushear' todas las etiquetas:

    git push --tags

'Pushear' una etiqueta específica:

    git push origin [tagname]


# Resolución de conflictos

Cuando varias personas intentan modificar la misma parte del mismo fichero (supongamos por ejemplo, los usuarios A y B), el segundo desarrollador que quiera incluir sus cambios (B) recibirá un mensaje de error de este estilo al hacer ``git pull``:

    CONFLICT (content):
    Merge conflict in README Automatic merge failed; fix conflicts and then commit the result.

En este caso el mensaje de error nos está indicando que hay un conflicto en el archivo README. Si abrimos el fichero con un editor de texto:

    This is the first line
    This is the second line
    <<<<<<< HEAD
    This is B's third line
    =======
    This is A's third line
    >>>>>>> 58828d47b756aaed335d0118d3b09a608b05bf5e

Lo que le está mostrando al usuario B el conflicto entre la versión del usuario A (commit 58828d47... ) y la versión del usuario B (HEAD, el commit en el que está el usuario).

Para arreglar el conflicto el usuario tiene que decidir manualmente con qué código final se queda para esa parte del fichero y eliminar las líneas de control “<<<...”, “===...”, “>>>...”, tras lo cual debe añadir el fichero con ``git add`` y hacer ``git commit`` normalmente.

Por ejemplo, el usuario podría modificar el fichero con conflicto para dejarlo de la siguiente manera:

    This is the first line
    This is the second line
    This is A and B's third line

Tras lo cual el conflicto estaría resuelto. No se añadirá el fichero al repositorio hasta que no se incluya el fichero en el mismo utilizando ``git add`` y ``git commit``.

# Branching y merging

A veces es necesario hacer cambios en una parte del código, sin afectar inmediatamente al resto del proyecto (por ejemplo, al añadir una funcionalidad nueva). Para esto, las herramientas de control de versiones suelen tener funcionalidades de branching (ramificar) y merging (fusionar).

Los comandos básicos para trabajar con ramas en ``git`` son:

Crear una nueva rama y cambiar a dicha rama. Esta nueva rama se creará a partir de la rama en la que estemos en ese momento. En este comando, ``-b`` significa 'new branch':

    git checkout -b mybranch

Git branch nos indicará qué ramas existen en el repositorio local:

    git branch

Y, si le pasamos como argumento ``-r`` (remote), nos indicará qué ramas existen en el repositorio remoto:

    git branch -r

Para cambiar a una rama ya existente (alguna de las que aparezcan en la salida de ``git branch``), usaremos git checkout:

    git checkout otherbranch

Usando el mismo comando, podemos volver a la rama que acabamos de crear:

    git checkout mybranch

Si creamos una rama localmente, para no perder trabajo normalmente querremos 'pushearla' al repo remoto. Al hacer ``git push`` desde esa rama, seguramente recibamos el siguiente mensaje de error:

    fatal: The current branch mybranch has no upstream branch.
    To push the current branch and set the remote as upstream, use

        git push --set-upstream origin mybranch

Ejecutando el comando que nos sugiere la herramienta, podemos pushear nueva la rama al repositorio remoto:

    git push --set-upstream origin mybranch

A partir de este momento, la nueva rama debería aparecer en la salida de ``git branch -r``:

    git branch -r

Adicionalmente, ahora que la rama existe en el repositorio remoto, para poder pushear a esa rama bastará con hacer ``git push`` desde la rama.

Se recuerda que ``git status`` siempre nos indicará en qué rama estamos. También es recomendable el uso del framework para ``zsh`` [Oh my zsh](https://ohmyz.sh), el cual siempre nos muestra en el shell el nombre de la rama en la que estemos, si estamos dentro de un repositorio ``git``.

Para fusionar (merge) dos ramas, debemos ir a la rama a la que queramos añadir los cambios:

    git checkout nombrerama

Y una vez en esa rama, invocar a ``git merge``, pasándole como argumento el nombre de la rama cuyos cambios queremos fusionar con la rama actual:

    git merge --no-ff ramaconcambios

La opción ``--no-ff`` obliga a que, en todo caso, se cree un 'merge commit' en la rama en la que hemos hecho el merge, preservando la topología de ramas que tengamos.


Si bien cada equipo de trabajo debe adoptar el esquema de branching y merging que le sea más útil, se puede encontrar un esquema muy interesante aquí: https://nvie.com/posts/a-successful-git-branching-model/ 

# Otros comandos útiles

Se recomienda leer sobre los comandos ``git blame`` y ``git bisect``, qué hacen y cómo se utilizan. ¿Cuándo podrían ser útiles dichos comandos en el contexto del desarrollo de proyectos?


# Agradecimientos
A [Luis Sanz](@QuirkyQuixote), por introducir el control de versiones en nuestro grupo de trabajo.

# Referencias
- Scott Chacon, “Pro Git”, http://git-scm.com/book
- Git reference: http://git-scm.com/docs

